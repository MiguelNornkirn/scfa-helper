package me.scfa.helper.scfahelper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ScfaHelper extends JavaPlugin implements Listener
{
    public final String STORAGE_FOLDER = "plugins" + File.separatorChar + "SCFAhelper" + File.separatorChar;
    public final String PT_HELP_FILE = "pt_help.txt";

    public int spawn_radius_x = 200;
    public int spawn_radius_z = 200;

    public static final double SPAWN_HEIGHT_Y = 256;

    public String[] whitelistedCommands =
            { "op", "deop", "help", "actualhelp", "ajuda", "ajudareal", "kit", "kitlist", "discord", "tell", "r", "tps",
                    "ignore", "changepassword", "mudarsenha", "joindate" };

    public Integer command = 0;

    public Random rng;

    // for preventing troll command spam
    private Map<String, Boolean> usedTrollHelp = new HashMap<>();
    private Map<String, Boolean> usedTrollDupe = new HashMap<>();

    private String ptHelpText;

    @Override
    public void onEnable()
    {

        System.out.println("Starting SCFAhelper");

        File theDir = getDataFolder();
        if (!theDir.exists())
            theDir.mkdirs();

        reloadTextConfig();

        this.saveDefaultConfig();
        refreshConfig();

        rng = new Random();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable()
    {
        System.out.println("Disabling SCFAhelper");
    }

    public void refreshConfig()
    {
        this.reloadConfig();

        spawn_radius_x = this.getConfig().getInt("region_size.size_x");
        spawn_radius_z = this.getConfig().getInt("region_size.size_z");
    }

    private String textRead(String PATH)
    {

        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines(Paths.get(PATH), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    private void reloadTextConfig()
    {

        File pt = new File(STORAGE_FOLDER + PT_HELP_FILE);

        try
        {
            pt.createNewFile();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        ptHelpText = textRead(STORAGE_FOLDER + PT_HELP_FILE);
        System.out.println(ptHelpText);
    }

    private boolean isPortuguese(Player p)
    {
        return true;
/*        if (p.getLocale().equals("pt_br") || p.getLocale().equals("pt_pt"))
            return true;
        else
            return false;*/
    }

    private void notAgain(Player p)
    {

        if (isPortuguese(p))
            p.sendMessage(ChatColor.RED + "Denovo não!");
        else
            p.sendMessage(ChatColor.RED + "Not again!");
    }

    private void notConsoleCmd()
    {
        System.out.println("This command isn't meant to be used by the console!");
    }

    private boolean usedWhitelistedCommands(String cmd)
    {
        for (String wc : whitelistedCommands)
        {
            if (cmd.equalsIgnoreCase(wc))
            {
                return true;
            }
        }
        return false;
    }

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event)
    {

        command = Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() { // delay for fixing keep
            // inventory glitch

            @Override
            public void run()
            {
                event.getEntity().spigot().respawn();
            }
        }, 1l);

    }

    @EventHandler
    public void onPlayerRespawnEvent​(PlayerRespawnEvent event)
    {

        Player player = event.getPlayer();
        Location spawn_location = event.getRespawnLocation();

        if (!event.isBedSpawn())
        {
            World world = spawn_location.getWorld();
            int pos_x = (rng.nextInt(spawn_radius_x * 2) - spawn_radius_x);
            int pos_y = 255;
            int pos_z = (rng.nextInt(spawn_radius_z * 2) - spawn_radius_z);

            boolean found_floor_level = false;
            while (!found_floor_level) {

                Block b = world.getBlockAt(pos_x, pos_y, pos_z);

                if (!b.isEmpty() || pos_y <= 0) {
                    found_floor_level = true;
                } else {
                    pos_y--;
                }

            }

            spawn_location.setX(pos_x);
            spawn_location.setY(pos_y);
            spawn_location.setZ(pos_z);

            if (player.getLocale().equals("pt_br") || player.getLocale().equals("pt_pt"))
            {
                player.sendMessage(
                        ChatColor.GREEN + "Você foi teleportado para um lugar aleatório na região do spawn!");
            } else
            {
                player.sendMessage(ChatColor.GREEN + "You respawned in a random point of the spawn region!");
            }


        }

    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {

        if (label.equalsIgnoreCase("op"))
        {
            if (sender instanceof Player)
            {

                sender.sendMessage(ChatColor.RED + "Console only!");
                return false;

            } else
            {

                if (args.length != 1)
                {
                    sender.sendMessage("Invalid arguments!");
                    return false;
                }

                getServer().getPlayer(args[0]).setOp(true);

                sender.sendMessage("OP set!");
                return true;
            }
        }

        if (label.equalsIgnoreCase("deop"))
        {
            if (sender instanceof Player)
            {

                sender.sendMessage(ChatColor.RED + "Console only!");
                return false;

            } else
            {

                if (args.length != 1)
                {
                    sender.sendMessage("Invalid arguments!");
                    return false;
                }

                getServer().getPlayer(args[0]).setOp(false);

                sender.sendMessage("OP removed!");
                return true;
            }
        }

        if (label.equalsIgnoreCase("kill") || label.equalsIgnoreCase("suicide") || label.equalsIgnoreCase("suicidio"))
        {

            if (sender instanceof Player)
            {

                Player player = (Player) sender;
                player.setHealth(0);

                if (isPortuguese(player))
                {
                    player.sendMessage(ChatColor.RED + "Você se matou!");
                } else
                {
                    player.sendMessage(ChatColor.RED + "You killed yourself!");
                }

                return true;
            } else
            {
                notConsoleCmd();
                return true;
            }

        }

        if (label.equalsIgnoreCase(this.getName()))
        { // reload

            if (args.length == 0)
            {
                sender.sendMessage(ChatColor.RED + "Too few arguments!");
                return false;
            } else
            {

                if (args[0].equalsIgnoreCase("reload"))
                {

                    if (sender.isOp())
                    {

                        sender.sendMessage(ChatColor.GREEN + "[" + this.getName() + "] Reloading config...");
                        refreshConfig();
                        return true;

                    } else
                    {

                        sender.sendMessage(ChatColor.RED + "You don't have permission to use this command!");
                        return false;
                    }
                }

                // if no matching command if found
                sender.sendMessage(ChatColor.RED + "Invalid arguments!");
                return false;
            }

        }

        if (label.equalsIgnoreCase("Help") || label.equalsIgnoreCase("Ajuda"))
        {

            if (sender instanceof Player)
            {

                Player senderPlayer = (Player) sender;
                Collection<? extends Player> allPlayers = Bukkit.getOnlinePlayers();

                if (!usedTrollHelp.containsKey(senderPlayer.getDisplayName()))
                    usedTrollHelp.put(senderPlayer.getDisplayName(), false);

                if (!usedTrollHelp.get(senderPlayer.getDisplayName()))
                {
                    for (Player player : allPlayers)
                    {
                        if (isPortuguese(player))
                        {
                            player.sendMessage(ChatColor.YELLOW + senderPlayer.getDisplayName()
                                    + " precisa de ajuda em " + String.valueOf(senderPlayer.getLocation().getBlockX())
                                    + " " + String.valueOf(senderPlayer.getLocation().getBlockY()) + " "
                                    + String.valueOf(senderPlayer.getLocation().getBlockZ()) + "!");
                        } else
                        {
                            player.sendMessage(ChatColor.YELLOW + senderPlayer.getDisplayName() + " needs help in "
                                    + String.valueOf(senderPlayer.getLocation().getBlockX()) + " "
                                    + String.valueOf(senderPlayer.getLocation().getBlockY()) + " "
                                    + String.valueOf(senderPlayer.getLocation().getBlockZ()) + "!");
                        }
                    }

                    if (isPortuguese(senderPlayer))
                    {
                        senderPlayer.sendMessage("Tenta /ajudareal na próxima!");
                    } else
                    {
                        senderPlayer.sendMessage("Try /actualhelp next time!");
                    }

                    usedTrollHelp.put(senderPlayer.getDisplayName(), true);

                } else
                {
                    notAgain(senderPlayer);
                    return false;
                }

                return true;
            } else
            {
                notConsoleCmd();
                return true;
            }

        }

        if (label.equalsIgnoreCase("ActualHelp") || label.equalsIgnoreCase("AjudaReal"))
        {
            sender.sendMessage(ChatColor.GREEN + ptHelpText);
        }

        if (label.equalsIgnoreCase("Dupe") || label.equalsIgnoreCase("Dupar"))
        {

            if (sender instanceof Player)
            {

                Player playerSender = (Player) sender;
                Collection<? extends Player> allPlayers = Bukkit.getOnlinePlayers();

                if (!usedTrollDupe.containsKey(playerSender.getDisplayName()))
                    usedTrollDupe.put(playerSender.getDisplayName(), false);

                if (!usedTrollDupe.get(playerSender.getDisplayName()))
                {
                    for (Player player : allPlayers)
                    {
                        if (isPortuguese(player))
                        {
                            player.sendMessage(ChatColor.YELLOW + playerSender.getDisplayName() + " tentou dupar em "
                                    + String.valueOf(playerSender.getLocation().getBlockX()) + " "
                                    + String.valueOf(playerSender.getLocation().getBlockY()) + " "
                                    + String.valueOf(playerSender.getLocation().getBlockZ()) + "!");
                        } else
                        {
                            player.sendMessage(ChatColor.YELLOW + playerSender.getDisplayName() + " tried to dupe in "
                                    + String.valueOf(playerSender.getLocation().getBlockX()) + " "
                                    + String.valueOf(playerSender.getLocation().getBlockY()) + " "
                                    + String.valueOf(playerSender.getLocation().getBlockZ()) + "!");
                        }
                    }

                    usedTrollDupe.put(playerSender.getDisplayName(), true);
                    return true;
                } else
                {
                    notAgain(playerSender);
                    return false;
                }
            } else
            {
                notConsoleCmd();
                return true;
            }

        }

        return false;

    }

}